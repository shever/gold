<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    public $fillable = [
        'receipt', 'name', 'phone',
        'region', 'city', 'email',
    ];
}
