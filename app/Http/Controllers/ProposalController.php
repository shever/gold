<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProposalRequest;
use App\Models\Proposal;

class ProposalController extends Controller
{
    public function index()
    {
        $proposals = Proposal::paginate(10);
        return view('proposals.index', compact('proposals'));
    }

    public function store(ProposalRequest $request)
    {
        $status = Proposal::create($request->all());
        return compact('status');
    }
}
