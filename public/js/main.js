/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "public";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//Style

__webpack_require__(1);

document.addEventListener('DOMContentLoaded', function () {

    var menu = void 0,
        scrollToReg = void 0,
        sendFormAction = void 0;

    Inputmask("(999){+|1}", { numericInput: true, placeholder: "" }).mask(document.querySelector('#receipt_promo'));
    Inputmask({ "mask": "+38(999)999-99-99" }).mask(document.querySelector('#phone_promo'));

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function checkPhone(phone) {
        var reg = /^(38|\+38){0,1}0[0-9]{9}/;
        return reg.test(phone);
    }

    //Listener resize window
    window.onresize = function (e) {
        var width = e.target.innerWidth,
            mobileSize = 768;

        if (width >= mobileSize) {
            menu.closeMenu();
        }
    };

    menu = {
        MENU_HAMBURGER: document.querySelector('.wrapp-hamb'),
        toggleMenuInit: function toggleMenuInit() {

            if (this.MENU_HAMBURGER != null) {
                this.MENU_HAMBURGER.addEventListener('click', function () {

                    document.querySelector('.header-wrapp').classList.toggle('open');
                });
            }
        },
        closeMenu: function closeMenu() {
            document.querySelector('.header-wrapp').classList.remove('open');
        }
    };

    scrollToReg = {
        ACTION_BUTTON: document.querySelector('.btn-anchorreg'),
        handlerScroll: function handlerScroll() {
            if (this.ACTION_BUTTON != null) {
                this.ACTION_BUTTON.addEventListener('click', function (e) {
                    e.preventDefault();
                    document.querySelector('.promotions div.form').scrollIntoView({ behavior: "smooth" });
                });
            }
        }
    };

    sendFormAction = {
        SEND_BUTTON: document.querySelector('.promotions .send-form'),
        handlerSendForm: function handlerSendForm() {
            if (this.SEND_BUTTON != null) {
                this.SEND_BUTTON.addEventListener('click', function (e) {
                    e.preventDefault();
                    sendFormAction.loader(true);
                    sendFormAction.clearErrorMessage;

                    var error = false,
                        receipt_promo = document.querySelector('#receipt_promo'),
                        name_promo = document.querySelector('#name_promo'),
                        phone_promo = document.querySelector('#phone_promo'),
                        email_promo = document.querySelector('#email_promo');

                    //Check number receipt
                    if (receipt_promo.value == "") error = true;
                    //Check name
                    if (name_promo.value.trim().length == 0) error = true;
                    //Check phone
                    if (phone_promo.inputmask.unmaskedvalue().length < 10) error = true;
                    //Check select region
                    if (townList.getValue().placeholder) error = true;
                    //Check select city
                    if (cityList.getValue().placeholder) error = true;
                    //Check email
                    if (email_promo.value.length != 0 && validateEmail(email_promo.value) == false) error = true;

                    if (error) {
                        document.querySelector('.error-form-promo').classList.add('show');
                        sendFormAction.loader(false);
                        return;
                    } else {
                        document.querySelector('.error-form-promo').classList.remove('show');
                    }

                    var regForm = new FormData( document.forms.reg_form );
                        regForm.append('region',    townList.getValue().label );
                        regForm.append('city',      cityList.getValue().label );

                    // SEND FORM 
                    // here don't forget remove loader 
                    // sendFormAction.loader(false);

                    // townList.setChoiceByValue('Область*');
                    //             cityList.setChoiceByValue('Мicто*').disable();
                    //             removeAddedCityList();
                    fetch( '/proposal/store', {
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token').getAttribute('content'),
                        },
                        body:   regForm,
                    })
                        .then(function(response){ 
                            if ( response.status != 200 ) {
                                document.querySelector('.error-form-promo-server').classList.add('show');
                            } else {
                                document.querySelector('.form-promo-success').classList.add('show');
                                receipt_promo.value = "",
                                name_promo.value    = "",
                                phone_promo.value   = "",
                                email_promo.value   = "";
                                townList.setChoiceByValue('Область*');
                                cityList.setChoiceByValue('Мicто*').disable();
                            }

                            sendFormAction.loader(false);
                        })
                        .catch( error => console.log( error ) )

                });
            }
        },
        loader: function loader(state) {
            state ? document.querySelector('.promotions .loader').classList.add('show-loader') : document.querySelector('.promotions .loader').classList.remove('show-loader');
        },
        clearErrorMessage: function() {
            var regFormError = document.querySelectorAll('.info-reg-form');

            for ( var i = 0; i < regFormError.length; i++ ) {
                regFormError[i].classList.remove( 'show' );
            }
        },
    };

    // Region dropdown
    var townList = new Choices('#choices-single-region', {
        placeholderValue: 'Область *',
        searchPlaceholderValue: 'Почнiть писати',
        loadingText: 'Завантаження...',
        noResultsText: 'Жодного результату не знайдено',
        itemSelectText: 'Натиснiть для вибору',
        classNames: {
            containerOuter: 'choices choices-fix-aev',
            containerInner: 'choices__inner input-base choices__inner-fix-aev',
            listSingle: 'choices__list--single choices__list-fix-aev',
            item: 'choices__item choices__item-fix-aev',

        }
    }).ajax(function (callback) {
        //Here we get list region
        fetch('https://api.hh.ru/areas/5').then(function (response) {
            response.json().then(function (data) {
                var allUkrData = data;
                //This part add to dropdown only areas of Ukraine
                data.areas.forEach(function (item) {
                    callback(item, 'id', 'name');
                });

                //Add placeholder after load town
                townList.setChoiceByValue('Область *');

                //
                document.getElementById('choices-single-region')
                    .addEventListener('change', function(e) {
                        var currentValue = townList.getValue();
                        console.log( currentValue )

                        if ( currentValue.choiceId !== 1 ) {
                            cityList.disable().clearStore();

                            allUkrData.areas.forEach(function(itemReg, regI){
                                if ( itemReg.id == currentValue.value ) {
                                    addPlaceholderToCity();
                                    itemReg.areas.forEach(function(item) {
                                        cityList.setValue([
                                            {value: item.id , label: item.name },
                                        ]);                                         
                                    })

                                    cityList.setChoiceByValue('Мicто*').enable();
                                }
                            })
                        } else {
                            cityList.disable().clearStore();
                            addPlaceholderToCity();
                            cityList.setChoiceByValue('Мicто*');
                        }
                    }, false);
            });
        }).catch(function (error) {
            console.error(error);
        });
    });

    // Region dropdown
    var cityList = new Choices('#choices-single-city', {
        placeholderValue: 'Мicто*',
        searchPlaceholderValue: 'Почнiть писати',
        loadingText: 'Завантаження...',
        noResultsText: 'Жодного результату не знайдено',
        itemSelectText: 'Натиснiть для вибору',
        classNames: {
            containerOuter: 'choices choices-fix-aev',
            containerInner: 'choices__inner input-base choices__inner-fix-aev',
            listSingle: 'choices__list--single choices__list-fix-aev',
            item: 'choices__item choices__item-fix-aev',

        }
    }).disable()

    function addPlaceholderToCity() {
        cityList.setValue([
            {
                label: "Мicто*",
                placeholder: true,
                value: "Мicто*"
            },
        ]);
    }

    //Flipflop counter
    new FlipDown(new Date(2019, 1, 27).getTime() / 1000, {
        theme: 'light'
    }).start();

    //Init
    menu.toggleMenuInit();
    scrollToReg.handlerScroll();
    sendFormAction.handlerSendForm();
});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);