@extends('layouts.app')

@section('content')
    <div class="content">
        <section class="banner">
            <div class="container">
                <div class="content-cnt">
                    <img src="/images/collection_1.jpg">
                </div>
            </div>      
        </section>
        <section class="participate">
            <div class="container">
                <div class="content-cnt">
                    <div class="participate-wrapp">
                        <h2>Як прийняти участь?</h2>
                        <h3>Щоб взяти участь вам потрібно зробити <br/> всього три простих кроки</h3>
                        <div class="wrapp-steps">
                            <div class="item">
                                <div class="item_img">
                                    <img src="/images/step1_1.png">
                                    <img class="shadow" src="/images/step1_2.png" style="bottom: -35px">
                                </div>
                                <div class="item_info">
                                    <p class="step_number">Крок 1</p>
                                    <p>Зробіть покупку  у будь-якому <br/> магазині <span class="accent">PROSTOR</span><br/> до <b>27.02.2019 р.</b></p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <img src="/images/step2.png">
                                </div>
                                <div class="item_info">
                                    <p class="step_number">Крок 2</p>
                                    <p>Зареєструй код чека</p>
                                    <a href="#" class="btn btn-anchorreg"><span>Реєстрація</span></a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <img src="/images/step3_1.png">
                                    <img class="shadow" src="/images/step3_2.png" style="bottom: -62px; left: 26px;">
                                </div>
                                <div class="item_info">
                                    <p class="step_number">Крок 3</p>
                                    <p>Візьми участь в розіграші <b>підвіски</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="promotions">
            <div class="container">
                <div class="content-cnt">
                    <div class="promotions-wrapp">
                        <div class="wrap-flipclock">
                            <h3>АКЦIЯ</h3>
                            <h4>Заповніть форму<br/> та прийміть участь у розіграші</h4>
                            <div id="flipdown" class="flipdown flipdown-fix-aev"></div>
                        </div>
                        <div class="wrap-form">
                            <div class="form">
                                <div class="loader">
                                    <div class="lds-dual-ring"></div>
                                </div>
                                <div class="form-title"><p>Реєстрація</p></div>
                                <form id="reg_form">
                                    <input class="input-base" type="text" name="receipt" placeholder="Номер чека*" required id="receipt_promo">
                                    <input class="input-base" type="text" name="name" placeholder="Ім’я*" required id="name_promo">
                                    <input class="input-base" type="tel" name="phone" placeholder="Телефон*" required id="phone_promo">
                                    <select name="" id="choices-single-region" placeholder>
                                        <option placeholder>Область*</option>
                                    </select>
                                    <select name="" id="choices-single-city" placeholder disabled>
                                        <option placeholder>Мicто*</option>
                                    </select>
                                    <!-- <input class="input-base" type="select-single" name="phone" placeholder="Місто*" required id="town-list"> -->
                                    <input class="input-base" type="email" name="email" placeholder="Email" id="email_promo">
                                    <p class="info-reg-form error-form-promo">Будь-ласка, заповнiть корректно усi поля позначенi (*) </p>
                                    <p class="info-reg-form error-form-promo-server">Реєстрація не завершена. Будь ласка, спробуйте пiзнiше</p>
                                    <p class="info-reg-form form-promo-success">Данi успiшно збереженi!</p>
                                    <button class="btn send-form" type="submit"><span>Реєстрація</span></button>
                                    <p class="agreement-form">Користувач, залишаючи заявку дає свою <br/> згоду на обробку персональних даних</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </section>
        <section class="best_offer">
            <div class="container">
                <div class="content-cnt">
                    <div class="wrapp-offers">
                        <h3>Краща пропозиція</h3>
                        <div class="wrapp-offers-item">
                            <div class="item">
                                <a href="#">
                                    <div class="image-wrapp">
                                        <img class="item-img" src="/images/example1.png" alt="img">
                                    </div>
                                    <p class="new_price">2 479 <span>грн.</span></p>
                                    <div class="old_price"><p>6 633 <span>грн.</span></p></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <div class="image-wrapp">
                                        <img class="item-img" src="/images/example2.png" alt="img">
                                    </div>
                                    <p class="new_price">1 999 <span>грн.</span></p>
                                    <div class="old_price"><p>5 888 <span>грн.</span></p></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <div class="image-wrapp">
                                        <img class="item-img" src="/images/example3.png" alt="img">
                                    </div>
                                    <p class="new_price">2 589 <span>грн.</span></p>
                                    <div class="old_price"><p>8 620 <span>грн.</span></p></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="#">
                                    <div class="image-wrapp">
                                        <img class="item-img" src="/images/example4.png" alt="img">
                                    </div>
                                    <p class="new_price">1 275 <span>грн.</span></p>
                                    <div class="old_price"><p>3 843 <span>грн.</span></p></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </section>
    </div>
@endsection
