@extends('layouts.app')

@section('content')
	<div class="content">
	    <section>
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Номер Чека</th>
						<th>Имя</th>
						<th>Телефон</th>
						<th>Область</th>
						<th>Город</th>
						<th>Email</th>
						<th>Дата</th>
					</tr>
				</thead>
				<tbody>
					@foreach($proposals as $proposal)
						<tr>
							<td>{{ $proposal->id }}</td>
							<td>{{ $proposal->receipt }}</td>
							<td>{{ $proposal->name }}</td>
							<td>{{ $proposal->phone }}</td>
							<td>{{ $proposal->region }}</td>
							<td>{{ $proposal->city }}</td>
							<td>{{ $proposal->email }}</td>
							<td>{{ $proposal->created_at }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="pagination">
				{!! $proposals->links() !!}
			</div>
		</section>
	</div>
@endsection