<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,600,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('js/choices.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/flipdown.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/choices.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/inputmask.dependencyLib.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/inputmask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/flipflop.js') }}"></script>

    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
    <header>
        <div class="container">
            <div class="content-cnt">
                <div class="header-wrapp">
                    <div class="logo">
                        <a href="/"><img class="logo-header" src="/images/logo.png"></a>
                        <div class="wrapp-hamb">
                            <span></span>
                        </div>
                    </div>
                    <div class="menu">
                        <ul class="menu-link">
                            <li><a href="#">Акція</a></li>
                            <li><a href="#">Магазини</a></li>
                            <li><a href="#">Колекція</a></li>
                            @auth
                                <li><a href="/proposals">Заявки</a></li>
                            @endauth
                        </ul>
                        <a class="menu-tel" href="tel:0 800 75 92 29">0 800 75 92 29</a>
                    </div>
                    
                </div>
            </div>
         </div>
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="content-cnt">
                <div class="wrapp-footer">
                    <div><a href="/"><img class="footer-logo" src="/images/logo.png" alt=""></a></div>
                    <div class="copyright"><span>© УкрЗолото 2019</span></div>
                    <a class="footer-link" href="/"><b>ukrzoloto.ua</b></a>
                </div>
            </div>
        </div>      
    </footer>
    <script src="{{ asset('js/main.js') }}"></script>
    <!--   <div class="container">
      <div class="content-cnt">
      </div>
    </div> -->
</body>
</html>
